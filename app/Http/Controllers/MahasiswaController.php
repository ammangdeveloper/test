<?php

namespace App\Http\Controllers;

use App\Models\Student;
use Illuminate\Http\Request;

class MahasiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $mahasiswa = Student::all();
        return view('welcome', compact('mahasiswa'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => ['required'],
            'nim' => ['required', 'unique:mahasiswa,nim'],
            'jk' => ['required'],
            'prodi' => ['required'],
            'fakultas' => ['required'],
        ]);

        Student::create([
            'nama' => $request->nama,
            'nim' => $request->nim,
            'jk' => $request->jk,
            'prodi' => $request->prodi,
            'fakultas' => $request->fakultas,
        ]);

        return redirect('mahasiswa');
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $mahasiswa = Student::find($id);
        return view('detail', compact('mahasiswa'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $mahasiswa = Student::find($id);
        return view('edit', compact('mahasiswa'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => ['required'],
            'jk' => ['required'],
            'prodi' => ['required'],
            'fakultas' => ['required'],
        ]);
        $mahasiswa = Student::find($id);
        $mahasiswa->nama = $request->nama;
        $mahasiswa->jk = $request->jk;
        $mahasiswa->prodi = $request->prodi;
        $mahasiswa->fakultas = $request->fakultas;
        $mahasiswa->update();
        return redirect()->route('mahasiswa.show', $mahasiswa->id);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $mahasiswa = Student::find($id);
        $mahasiswa->delete();
        return back();
    }
}
