<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Belajar Laravel</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
</head>

<body>
    <div class="container mt-3">
        <div class="row justify-content-center">
            <div class="col-md-4">
                <div class="card p-3">
                    <h5 class="text-center mb-3">Detail Mahasiswa</h5>
                    <p>Nama : {{ $mahasiswa->nama }}</p>
                    <p>Nim : {{ $mahasiswa->nim }}</p>
                    <p>Jenis Kelamin : {{ $mahasiswa->jk }}</p>
                    <p>Prodi : {{ $mahasiswa->prodi }}</p>
                    <p>Fakultas : {{ $mahasiswa->fakultas }}</p>
                    <a href="{{ route('mahasiswa.edit', $mahasiswa->id) }}" class="btn btn-primary" >Edit Data</a>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous">
    </script>
</body>

</html>
