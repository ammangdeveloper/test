<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Belajar Laravel</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
</head>

<body>
    <div class="container mt-3">
        <div class="row justify-content-center">
            <div class="col-md-5">
                <div class="card p-3">
                    <h5 class="text-center mb-3">Form Tambah Mahasiswa</h5>
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form action="{{ route('mahasiswa.update', $mahasiswa->id) }}" method="post">
                        @csrf
                        @method('put')
                        <label for="">Nama</label>
                        <input type="text" placeholder="Nama" name="nama" value="{{ $mahasiswa->nama }}"
                        class="form-control" id="">
                        <label for="" class="mt-2">Nim</label>
                        <input type="number" disabled name="nim" class="form-control mt-2"
                        value="{{ $mahasiswa->nim }}" id="">
                        <label for="" class="mt-2">Jenis Kelamin</label>
                        <select name="jk" class="form-control mt-2" id="">
                            @if ($mahasiswa->jk == 'l')
                            <option value="l" selected>Lk</option>
                            <option value="p">Pr</option>
                            @else
                            <option value="l">Lk</option>
                            <option value="p" selected>Pr</option>
                            @endif
                        </select>
                        <label for="" class="mt-2">Prodi</label>
                        <input type="text" name="prodi" value="{{ $mahasiswa->prodi }}" placeholder="Prodi" class="form-control mt-2"
                        id="">
                        <label for="" class="mt-2">Fakultas</label>
                        <input type="text" name="fakultas"value="{{ $mahasiswa->fakultas }}" placeholder="Fakultas" class="form-control mt-2"
                            id="">
                        <div class="input-group">
                            <button type="submit" class="btn btn-primary mt-3">Save</button>
                            <a href="{{ route('mahasiswa.index') }}" class="btn btn-danger mt-3 ms-2">Cancel</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous">
    </script>

</body>

</html>
