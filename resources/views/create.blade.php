<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Belajar Laravel</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
</head>

<body>
    <div class="container mt-3">
        <div class="row justify-content-center">
            <div class="col-md-5">
                <div class="card p-3">
                    <h5 class="text-center mb-3">Form Tambah Mahasiswa</h5>
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form action="{{ route('mahasiswa.store') }}" method="post">
                        @csrf
                        <input type="text" placeholder="Nama" name="nama" class="form-control" id="">
                        <input type="number" name="nim" placeholder="NIM" class="form-control mt-2" id="">
                        <select name="jk" class="form-control mt-2" id="">
                            <option value="l">Lk</option>
                            <option value="p">Pr</option>
                        </select>
                        <input type="text" name="prodi" placeholder="Prodi" class="form-control mt-2"
                            id="">
                        <input type="text" name="fakultas" placeholder="Fakultas" class="form-control mt-2"
                            id="">
                        <div class="input-group">
                            <button type="submit" class="btn btn-primary mt-3">Save</button>
                            <a href="{{ route('mahasiswa.index') }}" class="btn btn-danger mt-3 ms-2">Cancel</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous">
    </script>

</body>

</html>
